# Welcome to your CDK TypeScript project
#Please do basic setup before working with this project
## Its related to aws cdk to create cloudwatch alarm and use its functionality like metrics,events and actions to work with EC2 instance, SNS topic.

#As part of this project we used CPUutlization metric to check throshold and it it reaches below 60% then trigger cloudwatch alarm and which then trigger sns action to create sns topic and also trigger ec2 action to reboot the ec2 instance.

- cd existing_repo
- git init
- git remote add origin https://gitlab.com/aws-cdk-typescript/aws-cloudwatch.git
- git branch -M main
- git add .
- git commit -m "my first commit message"
- git branch --set-upstream-to=origin/main
- Allowed to force push
- https://gitlab.com/aws-cdk-typescript/aws-cloudwatch/-/settings/repository#js-protected-branches-settings
- 
- git push -uf origin main



The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
