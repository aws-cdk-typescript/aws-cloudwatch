import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch'
import { Construct } from 'constructs';
import * as cdk from 'aws-cdk-lib';

/*
// Construct NetworkInOut
// AWS EC2 NetworkInOut metric to create alarm 
*/
export class NetworkInOut extends Construct {
    public constructor(stack: cdk.Stack, id: string){
        super(stack, id) 

    //create cpu utlization metric for ec2
    const NetworkInOutMetric = new cloudwatch.Metric({
        namespace: 'AWS/EC2',
        metricName: 'NetworkInOut'
    });
    new cloudwatch.Alarm(this, 'NetworkAlarm', {
        metric: NetworkInOutMetric,        
        threshold: 60,  //Adjust threshold as required
        evaluationPeriods: 1, //The number of periods over which data is compared to the specified threshold.
        comparisonOperator: cloudwatch.ComparisonOperator.LESS_THAN_THRESHOLD,
    });

    }
}