import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import { CPUUtilization } from './CPUUtilization';
import { EC2Construct } from './EC2Instance';

export class ExerciseCdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'ExerciseCdkQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });

      /*Usage autoscalling construct
    ** CloudWatch contruct for CPUUtilization
    */
    //const myCloudWatchStack = new CPUUtilization(this, 'MyCloudWatch')

    /*Usage Ec2 instance construct
    ** EC2 myEC2Instance creation
    */
    const myEc2Construct = new EC2Construct(this, 'myEC2Instance')

  }
}
