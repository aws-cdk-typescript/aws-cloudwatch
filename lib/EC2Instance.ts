import * as cdk from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import { Construct } from 'constructs';
import * as iam from 'aws-cdk-lib/aws-iam'


export class EC2Construct extends Construct {
    public constructor(stack: cdk.Stack, id: string){
        super(stack, id) 
    // VPC creation with cidr block
    const vpc = new ec2.Vpc(this, 'MyCdkVPC');

    // //EC2 instance creation
    const instance = new ec2.Instance(this, "myec2",{
        instanceType: ec2.InstanceType.of(ec2.InstanceClass.BURSTABLE2,ec2.InstanceSize.MICRO),
        machineImage: new ec2.AmazonLinuxImage(),
        vpc: vpc
        });

    // /**
    //  * Add userdata i.e user preferred commands that will run when  
    //  * ec2 instance start first time
    //  */

    instance.userData.addCommands(
        'sudo yum check-update',
        'sudo yum upgrade',
        'sudo yum install httpd',
        'sudo systemctl start httpd',
        'systemctl status httpd',
        'systemctl restart httpd'
    );

/**
 * The following demonstrates how to create a launch template with an Amazon Machine Image,
 * security group, and an instance profile.
 */
    const role = new iam.Role(this, 'Role', {
        assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
      });
      const instanceProfile = new iam.InstanceProfile(this, 'InstanceProfile', {
        role,
      });
    const template =new ec2.LaunchTemplate(this,'AwsLinuxLaunchTemplate',{
        machineImage: ec2.MachineImage.latestAmazonLinux2(),
        securityGroup:new  ec2.SecurityGroup(this, 'LaunchTemplateSG',{
            vpc:vpc
        }),
        instanceProfile,
    });
        // Output value after generating reource
    //   new cdk.CfnOutput(this, 'myec2',
    //   {
    //     value: instance.instanceId,
    //     exportName: 'instanceId'
    //   });
    }
}