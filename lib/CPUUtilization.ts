import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch'
import { Construct } from 'constructs';
import * as cdk from 'aws-cdk-lib';
import * as actions from 'aws-cdk-lib/aws-cloudwatch-actions';
import * as sns from 'aws-cdk-lib/aws-sns'
import { EmailSubscription } from 'aws-cdk-lib/aws-sns-subscriptions';
import * as ec2 from 'aws-cdk-lib/aws-ec2'

/*
// Construct CPUUtilization
// AWS EC2 cpu utilization metric to create alarm for throshold less than 60%
*/
export class CPUUtilization extends Construct {
    public constructor(stack: cdk.Stack, id: string){
        super(stack, id) 
        
    /**
     * create cpu utlization metric for ec2
     */      
    const cpuUtilizationMetric = new cloudwatch.Metric({
        namespace: 'AWS/EC2',
        metricName: 'CPUUtilization',
        dimensionsMap: { InstanceId: 'i-02e3878c599a53735'} //EC2 instanceID
    });
    /**
     * Create cpu utlization alarm
     */
    const cpuUtilizationAlarm = new cloudwatch.Alarm(this, 'CPUAlarm', {
        metric: cpuUtilizationMetric,
        threshold: 60,  //Adjust threshold as required
        evaluationPeriods: 1, //The number of periods over which data is compared to the specified threshold.
        comparisonOperator: cloudwatch.ComparisonOperator.LESS_THAN_THRESHOLD
    });

    /**
     * Create SNS for alarms to be sent to
     */
    const topic = new sns.Topic(this, 'cpu_alarms');
    cpuUtilizationAlarm.addAlarmAction(new actions.SnsAction(topic));

    // new sns.Subscription(this, 'EmailSubscription', {
    //     topic,
    //     endpoint: 'smtp.gmail.com',
    //     protocol: sns.SubscriptionProtocol.EMAIL
    //     //subscriptionRoleArn: 'arn:aws:iam::638448471146:role/ExerciseCdkStack-CustomVpcRestrictDefaultSGCustomR-F2CD15VFNUDF'
    //     //'arn:aws:iam::638448471146:role/ExerciseCdkStack-CustomVpcRestrictDefaultSGCustomR-F2CD15VFNUDF'
    // });

    //const emailAddress = new CfnParameter(this, 'email-param');
  //  topic.addSubscription(new EmailSubscription(emailAddress.valueAsString));

    /**
     * Create Alarm action to reboot ec2 instance
     * Attach a reboot when alarm triggers
     */
    cpuUtilizationAlarm.addAlarmAction(
       new actions.Ec2Action(actions.Ec2InstanceAction.REBOOT)
   );
    

    }
}